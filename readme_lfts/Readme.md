# Laravel 8 From Scratch
## Section 2 - The basics

### 5. How a route Loads a View

1. Demostración de como las rutas cargan una vista.

### 6. Include CSS and JavaScript

1. Aplicando CSS

![](images/1.png)

2. Aplicando JS

![](images/2.png)

### 7. Make a Route and Link to it

![](images/3.png)

### 8. Store Blog Posts as HTML Files

1. Creación de carpeta posts donde se guardarán los html utilizados a continuación para mostrarlos de una forma mas dinamica.


![](images/4.png)

### 9. Route WildCard Constraints

1. Validaciones a la hora de colocar la ruta.

### 10. Use Caching for Expensive Operations

1. Agregando función para hacer un remember del cache

### 11. Use the Filesystem Class to Read a Directory

1. Cargando posts dinamicamente

![](images/5.png)

### 12. Find a Composer Package for Post Metadata

1. Posteando la metadata que se encuentran en los resources\posts utilizando composer para instalar un paquete que ayudará
a mapear correctamente los datos.

        use Spatie\YamlFrontMatter\YamlFrontMatter


### 13. Collection Sorting and Caching Refresher
1. Guardando datos en cache

![](images/6.png)

2. Haciendo un sort por fecha de los posts

## Section 3 - Blade

### 14. Blade: The Absolute Basics

1. Explicación del porqué usar el **.blade** en las vistas, entonces cambiar los valores a que estén dentro de (*{{  valor  }}*)

### 15. Blade Layouts Two Ways

1. Explicación de las dos maneras de utilizar los layouts y la creación de una carpeta llamada components que tendrá dentro a **layout.blade.php**

### 16. A Few Tweaks and Consideration

1. Quitando el sort y creando otra función aparte por si falla a la hora de redireccionar.

### 17. Environment Files and Database Connections

1. Creando conexión con la base de datos.

### 18. Migrations: The Absolute Basics

1. php artisan migrate:rollback, hace como su nombre lo dice un rollback de todo.
2. php artisan migrate, este como ya se sabe crea las migraciones nuevas pero con los cambios realizados.
3. php artisan migrate:fresh, hace un drop de todos los tables y después hace de nuevo un migrate.

### 19. Eloquent and the Active Record Pattern

1. Insertando nuevos users utilizando el php artisan tinker, además demostración de las diferentes maneras de poder consultar datos.


### 20. Make a Post Model and Migration

1. Eliminando el modelo de post que estaba creado por uno creado desde el php artisan y cargando posts desde la base de datos.

### 21. Enloquent Updates and HTML Escaping

1. Ejemplo al usar tags HTML pero en la vista es solo {{ post->title }} al cargar la pagina solo mostrará el texto tal cual junto con los tags
en cambio al usar {!! post->title !!} este si va a entender los tags para renderizar los tags como debe ser.

### 22. 3 Ways to Mitigate Mass Assigment Vulnerabilities

1. Demostración de las manera de mitigar el Mass Assigment permitiendo lo que se puede cambiar (fillable), decir cuales no se pueden cambiar (guarded) o evitarlo desde SQL.

### 23. Route Model Binding

        insert into posts (slug, title, excerpt, body) values ('my-family-post', 'My Family Post', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent porta, eros quis congue feugiat, augue nunc eleifend lorem, et efficitur metus orci vel felis. Donec volutpat eleifend mollis. In sed commodo mauris, ut vulputate magna. Suspendisse potenti. Nulla lobortis turpis orci, eget ultricies mauris tincidunt at. Nullam tristique lectus risus, feugiat lacinia tellus rutrum ultricies. Integer lectus justo, accumsan at tincidunt ut, euismod et sem. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas commodo, diam aliquam accumsan ullamcorper, diam justo feugiat lorem, quis luctus neque quam eu risus.</p>');
                
        insert into posts (slug, title, excerpt, body) values ('my-work-post', 'My Work Post', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent porta, eros quis congue feugiat, augue nunc eleifend lorem, et efficitur metus orci vel felis. Donec volutpat eleifend mollis. In sed commodo mauris, ut vulputate magna. Suspendisse potenti. Nulla lobortis turpis orci, eget ultricies mauris tincidunt at. Nullam tristique lectus risus, feugiat lacinia tellus rutrum ultricies. Integer lectus justo, accumsan at tincidunt ut, euismod et sem. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas commodo, diam aliquam accumsan ullamcorper, diam justo feugiat lorem, quis luctus neque quam eu risus.</p>');

        insert into posts (slug, title, excerpt, body) values ('my-hobby-post', 'My Hobby Post', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent porta, eros quis congue feugiat, augue nunc eleifend lorem, et efficitur metus orci vel felis. Donec volutpat eleifend mollis. In sed commodo mauris, ut vulputate magna. Suspendisse potenti. Nulla lobortis turpis orci, eget ultricies mauris tincidunt at. Nullam tristique lectus risus, feugiat lacinia tellus rutrum ultricies. Integer lectus justo, accumsan at tincidunt ut, euismod et sem. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas commodo, diam aliquam accumsan ullamcorper, diam justo feugiat lorem, quis luctus neque quam eu risus.</p>');

1. Cargando datos via slug sin el id.

### 24. Your First Eloquent Relationship

1. Referenciando los posts con su respectivo category mediante un category_id en los posts.

### 25. Show All Posts Associated With a Category

1. Creando relación entre posts y categories por igual para poder acceder a los categories desde un post

![](images/7.png)

### 26. Clockwork, and the N+4 Problem

1. Arreglando el problema de ejecutar muchos queries para un mismo objetivo a solo uno que obtenga todos los categories.

![](images/8.png)

### 27. Database Seeding Saves Time

1. Cambios para relacionar Post con User ademas de configurar el DatabaseSeeder para que se rellene automaticamente la informacion al ejecutar el comando:
        
        php artisan db:seed

### 28. Turbo Boost With Factories

1. Cargando datos de manera dinamica añadiendo un CategoryFactor y un PostFactory.

### 29. View All Posts By An Author

1. Añadiendo la funcíón de buscar por author utilizando un username como clave.

### 30. Eager Load Relationships on an Existing Model

1. Optimizando el relacionamiento.

### 31. Convert the HTML and CSS to Blade

1. Insertando nuevo contenido desde:

        https://github.com/laracasts/Laravel-From-Scratch-HTML-CSS

### 32. Blade Components and CSS Grids

1. Haciendo uso de los datos de la db para aplicarlos en los nuevos estilos de posts.

### 33. Convert the Blog Post Page

1. Haciendo mas dinamico los datos en las vistas de los posts ademas de agregar un nuevo componente llamado: **category-button.blade.php**

### 34. A Small JavaScript Dropdown Detour

1. Episodio enfocado en hacer funcionar de la mejor manera el dropdown que se muestra al inicio para ver las categorias.

### 35. How To Extract a Dropdown Blade Component

1. Optimizando el dropdown y creando otros componentes para hacerlo mas dinamico.

### 36. Quick Tweaks and Clean-Up

1. Mejorando el front end quitando cosas innecesarias y haciendo el codigo mas simple.

### 37. Search (The Messy Way)

1. Dandole funcionalidad al search.

### 38. Search (The Cleaner Way)

1. Realizando la funcionalidad del search de la mejor manera haciendo uso de un controller

### 39. Advanced Eloquent Query Constraints

1. Aplicando Query mas avanzado.

### 40. Extract a Category Dropdown Blade Component

1. Mejorando el search y colocando los posts en un mismo directorio.

### 41. Author Filtering

1. Aplicando filtro por autor.

### 42. Merge Category and Search Queries

1. Haciendo merge de los queries del search. 

### 43. Fix a Confusing Eloquent Bug

1. Arreglando bug del Eloquent.

### 44. Laughably Simple Pagination

1. Creando paginacion.

### 45. Build a Register User Page

1. Agregando funcionamiento para registrar nuevos usuarios.

### 46. Automatic Passwwork Hashing With Mutators

1. Hasheando passwords.

### 47. Failed Validation and Old Input Data

1. Validation de datos viejos.

### 48. Show a Success Flash Message

1. Mostrando mensaje si todo salio bien.

### 49. Login and Logout

1. Creando funcionalidad para hacer login y logout.

### 50. Build the Log In Page

1. Haciendo build del Login.

### 51. Laravel Breeze Quick Peak

1. Optimizando codigo y detalles en el login.

### 52. Write the Markup for a Post Comment

1. Agregando un post-comment.blade y haciendo detalles.
### 53. Table Consistency and Foreing Key Constraints

1. Agregando Comment como controller, modelo y en los factories, entre otros cambios.

### 54. Make the Comments Section Dynamic

1. Haciendo la seccion de comentarios dinamicos.

### 55, 56, 57. Some Light Chapter Clean Up

1. Dandole fin a la parte de la seccion de comentarios.

### 58, 59, 60 -> 70 Finish.





        


